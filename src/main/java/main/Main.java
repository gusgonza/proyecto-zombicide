package main;

import java.util.ArrayList;
import java.util.Scanner;

import game.Game;
import players.Player;
import weapons.Arch;
import weapons.Axe;
import weapons.Spell;
import weapons.Sword;
import weapons.Weapon;
import zombie.Boomer;
import zombie.Runner;
import zombie.Walker;
import zombie.Zombie;

public class Main {

    protected static ArrayList<Player> players = new ArrayList<>();
    protected static ArrayList<Player> selectedPlayers = new ArrayList<>();
    protected static ArrayList<Zombie> zombies = new ArrayList<>();
    private static ArrayList<Weapon> weapons = new ArrayList<>();

    public static void initCharacters() {
        players.add(new Player("James", 7, 7, true, true, new Weapon("Mandoble", 2, 1, 4, 1)));
        players.add(new Player("Marie", 5, 5, true, true, new Weapon()));
        players.add(new Player("Jaci", 5, 5, true, true, new Weapon()));
    }

    public static void initZombies() {
        zombies.add(new Walker("Caminante", 1, 1, true, 2, 1, 'w'));
        zombies.add(new Boomer("Estallador", 1, 1, true, 2, 1, 'b'));
        zombies.add(new Runner("Corredor", 1, 1, true, 2, 1, 'r'));
    }

    public static void initObjects() {
        weapons.add(new Arch("Arco largo", 1, 2, 3, 1));
        weapons.add(new Axe("Hacha doble", 2, 1, 3, 1));
        weapons.add(new Spell("Bola de fuego", 1, 3, 4, 1));
        weapons.add(new Sword("Espada corta", 1, 1, 4, 1));
    }

    public static ArrayList<Zombie> getZombies() {
        return zombies;
    }

    public static Weapon getWeapon() {
        // Porcentaje de probabilidad de obtener un arma de alta calidad
        int percentageOfGodWeapon = (int) (Math.random() * 100) + 1;
        if (percentageOfGodWeapon > 50) { // Probabilidad del 50% para obtener un arma de alta calidad
            int randomWeaponIndex = (int) (Math.random() * weapons.size());
            return weapons.get(randomWeaponIndex);
        } else {
            // Si no se obtiene un arma de alta calidad, se devuelve una Daga (arma básica)
            return new Weapon();
        }
    }

    public static void main(String[] args) {
        initCharacters();
        ArrayList<Zombie> enemyZombies = new ArrayList<>();
        initZombies();
        initObjects();

        Scanner scanner = new Scanner(System.in);
        boolean exits = true;
        while (exits) {
            menuGame();
            int optionSelect = scanner.nextInt();
            switch (optionSelect) {
                case 0:
                    System.out.println("Fin del juego...");
                    exits = false;
                    break;
                case 1:
                    newGame();
                    break;
                case 2:
                    if (players.size() < 10) {
                        newCharacter();
                        continue;
                    } else {
                        System.out.println("Has alcanzado el número máximo de personajes");
                        continue;
                    }
                default:
                    System.out.println("* * * Opción incorrecta. Inténtalo de nuevo * * *\n");
            }
        }
    }

    private static void menuGame() {
        System.out.println("""
                ===== MENÚ =====
                0 - Salir
                1 - Nuevo Juego
                2 - Nuevo Personaje
                =================
                """);
    }

    public static void newGame() {
        Scanner scanner = new Scanner(System.in);
        Player character;

        if (players.size() >= 3) {
            if (players.size() == 3) {
                selectedPlayers.addAll(players);
                System.out.println("Personajes seleccionados: ");
                for (int i = 0; i < selectedPlayers.size(); i++) {
                    System.out.println(selectedPlayers.get(i).toString());
                }
                System.out.println("Tus personajes han sido seleccionados.");
                Game game = new Game(selectedPlayers);
                game.startRound();
            } else {
                System.out.println("Selecciona tus personajes (mínimo 3, máximo 6): ");
                System.out.println("Personajes disponibles: ");
                for (int i = 0; i < players.size(); i++) {
                    character = players.get(i);
                    System.out.println((i + 1) + " - " + character);
                }
                System.out.println("Ingresa los números de los personajes que deseas seleccionar (separados por espacios): ");
                String[] selections = scanner.nextLine().split(" ");
                for (String selection : selections) {
                    int index = Integer.parseInt(selection) - 1;
                    if (index >= 0 && index < players.size()) {
                        selectedPlayers.add(players.get(index));
                    }
                }
                if (selectedPlayers.size() < 3) {
                    System.out.println("Debes seleccionar al menos 3 personajes.");
                    selectedPlayers.clear();
                } else {
                    System.out.println("Tus personajes han sido seleccionados.");
                    Game game = new Game(selectedPlayers);
                    game.startRound();
                }
            }
        } else {
            System.out.println("No hay suficientes personajes creados.");
        }
    }

    public static void newCharacter() {
        System.out.println("Ingresa el nombre del personaje: ");
        String nameCharacter = new Scanner(System.in).nextLine();
        Player newPlayer = new Player(nameCharacter, 5, 5, true, false, getWeapon());
        players.add(newPlayer);
    }

}