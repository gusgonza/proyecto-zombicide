package game;

import main.Main;
import players.Player;
import weapons.*;
import zombie.*;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import static main.Main.initObjects;

/**
 * La clase Game representa la lógica principal del juego.
 */
public class Game {
    private final ArrayList<Player> players;
    private final ArrayList<Weapon> weapons = new ArrayList<>();
    private final ArrayList<Zombie> enemyZombies = new ArrayList<>();
    private int level = 0;

    /**
     * Constructor para la clase Game.
     *
     * @param players Lista de jugadores
     */
    public Game(ArrayList<Player> players) {
        this.players = players;
        setWeapons();
        initObjects();
        setLevel(players.size());
        startRound();
    }

    private void setWeapons() {
        weapons.add(new Arch(true));
        weapons.add(new Axe(true));
        weapons.add(new Spell(true));
        weapons.add(new Sword(true));
    }

    private void setLevel(int level) {
        this.level = level;
    }

    public void startRound() {
        Scanner scanner = new Scanner(System.in);
        int count = 0;
        int level = this.level;
        boolean exitRound = false;
        Random random = new Random();
        while (!exitRound) {
            if (level == 0) {
                Main.initZombies();
                this.enemyZombies.addAll(Main.getZombies());
            } else {
                spawnEnemyZombies();
            }
            int numberRound = 0;
            System.out.println("|------------------------------ Nivel: " + level + " : " + numberRound + " ------------------------------|");
            for (Zombie zombie : enemyZombies) {
                System.out.println("==| " + zombie.getName() + " (" + zombie.getTypeZombie() + ") |==");
            }
            for (Player player : players) {
                System.out.println("JUGADOR: El Superviviente " + player.getName() + " tiene " + player.getHealth() + " de salud máxima, usa " + player.getWeapon().getName() + " como arma y hace " + player.getWeapon().getDamage() + " de daño");
                System.out.println("""
                        0 - Pasar
                        1 - Atacar
                        2 - Habilidad Especial
                        3 - Buscar
                        4 - Cambiar Arma
                        """);

                int action = scanner.nextInt();
                switch (action) {
                    case 0:
                        break;
                    case 1:
                        if (!enemyZombies.isEmpty()) {
                            int zombieIndex = random.nextInt(enemyZombies.size());
                            Zombie zombie = enemyZombies.get(zombieIndex);
                            playerAttack(player, zombie);
                        } else {
                            System.out.println("No hay zombies para atacar.");
                        }
                        break;
                    case 2:
                        player.specialAttack(enemyZombies);
                        break;
                    case 3:
                        searchObject();
                        break;
                    case 4:
                        changeWeapon(player);
                        break;
                }
            }
            count++;
            if (count == players.size()) {
                count = 0;
                level++;
            }
        }
    }

    /**
     * Method to change the weapon of a player.
     *
     * @param player The player whose weapon is to be changed
     */
    private void changeWeapon(Player player) {
        System.out.println("Selecciona un arma:");
        for (int i = 0; i < weapons.size(); i++) {
            System.out.println(i + " - " + weapons.get(i).getName());
        }
        Scanner scanner = new Scanner(System.in);
        int weaponIndex = scanner.nextInt();
        if (weaponIndex >= 0 && weaponIndex < weapons.size()) {
            Weapon weapon = weapons.get(weaponIndex);
            player.setWeapon(weapon);
            System.out.println(player.getName() + " ha cambiado su arma a " + weapon.getName());
        } else {
            System.out.println("Índice de arma inválido. Por favor, intenta de nuevo.");
        }
    }

    /**
     * Method to search for an object in the game.
     */
    private void searchObject() {
        Random random = new Random();
        int objectIndex = random.nextInt(weapons.size());
        Weapon weapon = weapons.get(objectIndex);
        System.out.println("Has encontrado un objeto: " + weapon.getName());
    }

    /**
     * Method to spawn enemy zombies in the game.
     */
    private void spawnEnemyZombies() {
        Random random = new Random();
        int numZombies = level + 1;

        for (int i = 0; i < numZombies; i++) {
            int zombieType = random.nextInt(3);
            Zombie newZombie;

            switch (zombieType) {
                case 0:
                    newZombie = new Runner(true);
                    break;
                case 1:
                    newZombie = new Walker(true);
                    break;
                default:
                    newZombie = new Boomer(true);
                    break;
            }
            enemyZombies.add(newZombie);
        }
    }

    /**
     * Method for a player to attack a zombie.
     *
     * @param player The player who is attacking
     * @param zombie The zombie being attacked
     */
    public void playerAttack(Player player, Zombie zombie) {

        Weapon weapon = player.getWeapon();
        Random random = new Random();

        if (weapon.getDamage() >= zombie.getHealth()) {
            int hit = random.nextInt(6);
            if (hit >= weapon.getAccuracy()) {
                enemyZombies.remove(zombie);
                System.out.println(zombie.getTypeZombie() + " está muerto");
            } else {
                System.out.println(player.getName() + " ha fallado con un " + weapon.getDamage() + " contra " + zombie.getName() + " (" + zombie.getTypeZombie() + ")");
            }
        } else {
            System.out.println(player.getName() + " ha fallado el ataque contra " + zombie.getName() + " (" + zombie.getTypeZombie() + ")");
        }

        player.takeDamage(zombie.getDamageZombie());

        if (player.getHealth() <= 0) {
            player.setAlive(false);
            System.out.println("El jugador " + player.getName() + " ha muerto...");
            players.remove(player);
        }
    }

}
