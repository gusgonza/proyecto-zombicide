package players;

import humanoide.Humanoide;
import weapons.Weapon;
import zombie.Zombie;

import java.util.ArrayList;

/**
 * La clase Player extiende la clase Humanoide y representa un jugador en el juego.
 * Cada jugador tiene un arma y un estado roundPassed además de los atributos heredados de Humanoide.
 */
public class Player extends Humanoide {
    protected Weapon weapon;
    protected boolean roundPassed;

    /**
     * Constructor para la clase Player.
     *
     * @param name        El nombre del jugador
     * @param health      La salud actual del jugador
     * @param maxHealth   La salud máxima del jugador
     * @param alive       El estado de vida del jugador
     * @param roundPassed El estado de roundPassed del jugador
     * @param weapon      El arma que el jugador está sosteniendo
     */
    public Player(String name, int health, int maxHealth, boolean alive, boolean roundPassed, Weapon weapon) {
        super(name, health, maxHealth, alive);
        setRoundPassed(roundPassed);
        setWeapon(weapon);
    }

    /**
     * Getter para el atributo weapon.
     *
     * @return El arma que el jugador está sosteniendo actualmente
     */
    public Weapon getWeapon() {
        return weapon;
    }

    /**
     * Setter para el atributo weapon.
     *
     * @param weapon El arma a establecer
     */
    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    /**
     * Getter para el atributo roundPassed.
     *
     * @return El estado de roundPassed del jugador
     */
    public boolean isRoundPassed() {
        return roundPassed;
    }

    /**
     * Setter para el atributo roundPassed.
     *
     * @param roundPassed El estado de roundPassed a establecer
     */
    public void setRoundPassed(boolean roundPassed) {
        this.roundPassed = roundPassed;
    }

    /**
     * Método para que el jugador reciba daño.
     *
     * @param damage La cantidad de daño a recibir
     */
    public void takeDamage(int damage) {
        health -= damage;
        if (health <= 0) {
            health = 0;
            alive = false;
            System.out.println(name + " ha sido derrotado!");
        } else {
            System.out.println(name + " recibe " + damage + " de daño!");
        }
    }

    /**
     * Método para que el jugador realice un ataque especial.
     *
     * @param enemyZombies La lista de zombies enemigos a atacar
     */
    public void specialAttack(ArrayList<Zombie> enemyZombies) {
        if (weapon != null) {
            weapon.specialAttack(enemyZombies);
        }
    }

    /**
     * Método toString sobrescrito para la clase Player.
     *
     * @return Una representación en cadena del jugador
     */
    @Override
    public String toString() {
        return getName() + ", Salud: " + getHealth() + ", Arma: " + getWeapon();
    }
}